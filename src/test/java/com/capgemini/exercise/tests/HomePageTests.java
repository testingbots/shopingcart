package com.capgemini.exercise.tests;

import org.junit.Assert;
import org.junit.Test;
import com.capgemini.exercise.pages.HomePage;

public class HomePageTests {

	
	@Test
	public void validateOnHomePage() throws InterruptedException {
		
		HomePage homePage = new HomePage();
		Assert.assertEquals("My Store",homePage.getTitle());
        homePage.closeBrowser();
	}
}
