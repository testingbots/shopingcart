package com.capgemini.exercise.stepdefinitions;

import java.util.Random;

import org.apache.http.conn.ssl.BrowserCompatHostnameVerifier;
import org.junit.Assert;

import com.capgemini.exercise.pages.AccountCreationPage;
import com.capgemini.exercise.pages.BrowserFactory;
import com.capgemini.exercise.pages.HomePage;
import com.capgemini.exercise.pages.MyAccountPage;
import com.capgemini.exercise.pages.SignInOrCreateAccountPage;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


public class CreateAccountStepDefinitions {

	private String emailaddress;
	HomePage homePage=null;
    SignInOrCreateAccountPage signInOrCreateAccountPage=null;
    AccountCreationPage accountCreationPage=null;
	MyAccountPage myAccountPage=null;
	
//	@Before("@CreateAccount")
//	public void setUp() {
//		BrowserFactory.getBrowser();
	//homePage = new HomePage();
	
//	}
//	@After("@CreateAccount")
//	public void tearDown() {
//		homePage.closeBrowser();
//	}
	@Given("^user is on ecommerce webpage$")
	public void user_is_on_ecommerce_webpage() {
		 homePage = new HomePage();
		signInOrCreateAccountPage = homePage.signIn();
	}

	@When("^user provides all details$")
	public void user_provide_all_details() {
		emailaddress= randomString()+"@hotmail.com";
		accountCreationPage = signInOrCreateAccountPage.enterEmailandRegister(emailaddress);
	    accountCreationPage.providePseronalInformation("testfirstname", "testlastname", "pass123", "3-3-1983");
	    accountCreationPage.provideAddressInformation("21", "Alaska", "2", "50522", "United States","000000000000");
	    myAccountPage= accountCreationPage.register();
	}

	@Then("^user account should be created$")
	public void user_account_should_be_created() {
		Assert.assertTrue(myAccountPage.getSignOut().isDisplayed());
		myAccountPage.signOut();
	}
	
	@When("^user provides log in details$")
	public void user_provide_log_in_details(){
	  myAccountPage = signInOrCreateAccountPage.loginWith("s1s3@gmail.com", "pass123");
	}

	@Then("^user should login successfully$")
	public void user_should_login_successfully() {
		Assert.assertTrue(myAccountPage.getSignOut().isDisplayed());
		myAccountPage.signOut();
	}

	@When("^user adds expensive product to cart$")
	public void user_adds_expensive_product_to_cart(){
	  myAccountPage.addExpensiveDressToCart();
	}

	@Then("^the cart should have a product$")
	public void the_cart_should_have_a_product(){
	    Assert.assertTrue(myAccountPage.isProductAddedToCart());
	    myAccountPage.signOut();
	}
	
	@When("^user relogin$")
	public void user_relogin() {
	   homePage.signIn();
	   signInOrCreateAccountPage.loginWith("s1s3@gmail.com", "pass123");
	}
	
	@When("^go to cart$")
	public void go_to_cart() {
		myAccountPage.goToCart();
	}
	
	private String randomString() {
		int leftLimit = 97; // letter 'a'
	    int rightLimit = 122; // letter 'z'
	    int targetStringLength = 5;
	    Random random = new Random();
	    StringBuilder buffer = new StringBuilder(targetStringLength);
	    for (int i = 0; i < targetStringLength; i++) {
	        int randomLimitedInt = leftLimit + (int) 
	          (random.nextFloat() * (rightLimit - leftLimit + 1));
	        buffer.append((char) randomLimitedInt);
	    }
	    String generatedString = buffer.toString();
	 
	    return generatedString;
	
	}
	
}
