package com.capgemini.exercise.runner;

import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
 
@RunWith(Cucumber.class)
@CucumberOptions(
		features = "src/test/resources/features",
		glue= {"com/capgemini/exercise/stepdefinitions"},
		plugin = { "pretty", "html:target/reports/" },
		tags = {"@CreateAccount"},
		monochrome = true
		)
public class TestRunner {
 
}
