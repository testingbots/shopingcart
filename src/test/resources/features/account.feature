@CreateAccount    
Feature: Creating an account on ecommerce website
           As a User 
           I want to create an account 
           in order to do my online shopping
           
     Scenario: user creates an account on ecommerce website
			Given user is on ecommerce webpage
			When user provides all details
			Then user account should be created
			
			
		 Scenario: user login with newly created account
		 Given user is on ecommerce webpage
		 When user provides log in details
		 Then user should login successfully
		 
		 
		 Scenario: user adds most expensive product to cart
		 Given user is on ecommerce webpage
		 When user provides log in details
		 And user adds expensive product to cart
		 Then the cart should have a product
		 
		 
		 Scenario: The product added to cart should be available when uer logsout and log in again.
		 Given user is on ecommerce webpage
		 When user provides log in details
		 And user adds expensive product to cart
		 Then the cart should have a product
		 When user relogin
		 And go to cart
		 Then the cart should have a product