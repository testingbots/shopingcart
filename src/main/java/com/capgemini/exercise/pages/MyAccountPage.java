package com.capgemini.exercise.pages;

import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class MyAccountPage extends BasePage {

	public MyAccountPage() {
		Assert.assertEquals("My account - My Store", driver.getTitle());
	}
	
	public WebElement getSignOut() {
		WebElement signOut = driver.findElement(By.linkText("Sign out"));
		 return signOut;
	}
	
	public WebElement getDresses() {
		WebElement dresses = driver.findElement(By.xpath("//*[@id=\"block_top_menu\"]/ul/li[2]/a"));
		 return dresses;
	}
	
	public WebElement getCheckout() {
		WebElement checkout = driver.findElement(By.xpath("//*[@id=\"layer_cart\"]/div[1]/div[2]/div[4]/a/span"));
		return checkout;
	}
	
	public WebElement getCart() {
		WebElement cart = driver.findElement(By.xpath("//*[@id=\"header\"]/div[3]/div/div/div[3]/div/a"));
		return cart;
	}
	
	public void goToCart() {
		getCart().click();
	}
	public boolean isProductAddedToCart() {
//		WebElement quantity = driver.findElement(By.name("quantity_4_16_0_98381"));
		WebElement quantity=null;
		boolean isProductAdded=false;
		try {
		 quantity = driver.findElement(By.id("product_4_16_0_98381"));
		 if(quantity.isDisplayed()) {
			 isProductAdded=true;
		 }
		} catch (NoSuchElementException e) {
			return false;
		}
		return isProductAdded;
	}
	
	public void signOut() {
		getSignOut().click();
	}
	public void addExpensiveDressToCart(){
		getDresses().click();
		List <WebElement> products = driver.findElements(By.xpath("//*[@id=\"center_column\"]/ul/li"));
		double max =0.00 ;
		WebElement element = null;
		for (int i=1; i<=products.size();i++) {
			WebElement price = driver.findElement(By.xpath("//*[@id=\"center_column\"]/ul/li["+i+"]/div/div[2]/div[1]/span"));
			String finalPrice = price.getText().substring(1);
			if (Double.parseDouble(finalPrice) > max){
				 max = Double.parseDouble(finalPrice);
				 Actions actions = new Actions(driver);
				 actions.moveToElement(price).build().perform();
				 element = driver.findElement(By.xpath("//*[@id=\"center_column\"]/ul/li["+i+"]/div/div[2]/div[2]/a[1]/span"));
			}
		}
		element.click();
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.visibilityOf(getCheckout()));
		getCheckout().click();
	}
}
