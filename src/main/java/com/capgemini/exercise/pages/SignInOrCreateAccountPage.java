package com.capgemini.exercise.pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class SignInOrCreateAccountPage extends BasePage {

	
	public SignInOrCreateAccountPage() {
		Assert.assertEquals("Login - My Store", driver.getTitle());
	}
	/**
	 * @return the emailCreate
	 */
	public WebElement getEmailCreate() {
		WebElement emailCreate = driver.findElement(By.id("email_create"));
		return emailCreate;
	}
	
	/**
	 * @return the createAccount
	 */
	public WebElement getCreateAccount() {
		WebElement createAccount = driver.findElement(By.id("SubmitCreate"));
		return createAccount;
	}
	
	/**
	 * @param email
	 * @return AccountCreationPage
	 */
	public AccountCreationPage enterEmailandRegister(String email) {
		getEmailCreate().sendKeys(email);
		getCreateAccount().click();
		return new AccountCreationPage();
	}
	
	/**
	 * @return email
	 */
	public WebElement getEmailAddress() {
		WebElement email = driver.findElement(By.id("email"));
		return email;
	}
	
	/**
	 * @return password
	 */
	public WebElement getPassword() {
		WebElement password = driver.findElement(By.id("passwd"));
		return password;
	}
	
	/**
	 * @return signInButton
	 */
	public WebElement getSignInButton() {
		WebElement signInButton = driver.findElement(By.id("SubmitLogin"));
		return signInButton;
	}
 /**
 * @param username
 * @param password
 * @return MyAccountPage
 */
public MyAccountPage loginWith(String username,String password) {
	 getEmailAddress().sendKeys(username);
	 getPassword().sendKeys(password);
	 getSignInButton().click();
	 return new MyAccountPage();
 }
}
