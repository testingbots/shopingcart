package com.capgemini.exercise.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class AccountCreationPage extends BasePage {


	/**
	 * @return the mr
	 */
	public WebElement getMr() {
		WebElement mr = driver.findElement(By.id("id_gender1"));
		return mr;
	}

	/**
	 * @return the mrs
	 */
	public WebElement getMrs() {
		WebElement mrs = driver.findElement(By.id("id_gender2"));
		return mrs;
	}

	/**
	 * @return the firstName
	 */
	public WebElement getFirstName() {
		 WebElement firstName = driver.findElement(By.id("customer_firstname"));
		return firstName;
	}

	/**
	 * @return the lastName
	 */
	public WebElement getLastName() {
		WebElement lastName = driver.findElement(By.id("customer_lastname"));
		return lastName;
	}

	/**
	 * @return the days
	 */
	public WebElement getDay() {
		WebElement days = driver.findElement(By.id("days"));
		return days;
	}

	/**
	 * @return the month
	 */
	public WebElement getMonth() {
		WebElement months = driver.findElement(By.id("months"));
		return months;
	}

	/**
	 * @return the year
	 */
	public WebElement getYear() {
		WebElement years = driver.findElement(By.id("years"));
		return years;
	}

	/**
	 * @return the password
	 */
	public WebElement getPassword() {
		WebElement password = driver.findElement(By.id("passwd"));
		return password;
	}

	/**
	 * @return the address_firstname
	 */
	public WebElement getAddress_firstname() {
		WebElement address_firstname = driver.findElement(By.id("firstname"));
		return address_firstname;
	}

	/**
	 * @return the address_lastname
	 */
	public WebElement getAddress_lastname() {
		WebElement address_lastname = driver.findElement(By.id("lastname"));
		 return address_lastname;
	}

	/**
	 * @return the address_firstline
	 */
	public WebElement getAddress_firstline() {
		WebElement address_firstline = driver.findElement(By.id("address1"));
		return address_firstline;
	}

	/**
	 * @return the city
	 */
	public WebElement getCity() {
		WebElement city = driver.findElement(By.id("city"));
		return city;
	}

	/**
	 * @return the state
	 */
	public WebElement getState() {
		WebElement state = driver.findElement(By.id("id_state"));
		return state;
	}

	/**
	 * @return the postalCode
	 */
	public WebElement getPostalCode() {
		WebElement postalCode = driver.findElement(By.id("postcode"));
		return postalCode;
	}

	/**
	 * @return the country
	 */
	public WebElement getCountry() {
		WebElement country = driver.findElement(By.id("id_country"));
		return country;
	}

	/**
	 * @return the mobilePhone
	 */
	public WebElement getMobilePhone() {
		WebElement mobilePhone = driver.findElement(By.id("phone_mobile"));
		return mobilePhone;
	}

	/**
	 * @return the register
	 */
	public WebElement getRegister() {
		WebElement register = driver.findElement(By.id("submitAccount"));
		return register;
	}

	/**
	 * @return MyAccountPage
	 */
	public MyAccountPage register() {
	    getRegister().click();
		return new MyAccountPage();
	}
	
	/**
	 * @param firstname
	 * @param lastname
	 * @param password
	 * @param dob
	 */
	public void providePseronalInformation(String firstname,String lastname,String password, String dob) {
		if(!getMr().isSelected()) {
			getMr().click();
		}
		getFirstName().sendKeys(firstname);
		getLastName().sendKeys(lastname);
		getPassword().sendKeys(password);
		Select days = new Select(getDay());
		days.selectByValue(dob.split("-")[0]);
		Select month = new Select(getMonth());
		month.selectByValue(dob.split("-")[1]);
		Select year = new Select(getYear());
		year.selectByValue(dob.split("-")[2]);
	}
	
	/**
	 * @param address1
	 * @param city
	 * @param state
	 * @param postCode
	 * @param country
	 * @param mobile
	 */
	public void provideAddressInformation(String address1,
			String city,String state, String postCode,String country,String mobile) {
		
		getAddress_firstline().sendKeys(address1);
		getCity().sendKeys(city);
		Select sta = new Select(getState());
		sta.selectByValue("2");
		getPostalCode().sendKeys(postCode);
		Select coun = new Select(getCountry());
		coun.selectByVisibleText(country);
		getMobilePhone().sendKeys(mobile);
		
	}
	
	
}
