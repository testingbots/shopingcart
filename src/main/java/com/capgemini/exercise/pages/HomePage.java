package com.capgemini.exercise.pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class HomePage extends BasePage {

	public HomePage(){
		driver.navigate().to(System.getProperty("baseUrl"));
		Assert.assertEquals("My Store", driver.getTitle());
	}
	
	/**
	 * @return title
	 */
	public String getTitle() {
		return driver.getTitle();
	}
	
	/**
	 * @return singleLink
	 */
	public WebElement getSignInLink() {
		WebElement signInLink =  driver.findElement(By.linkText("Sign in"));
		  return signInLink;
	}
	
   /**
 * @return SignInOrCreateAccountPage
 */
public SignInOrCreateAccountPage signIn() {
	 getSignInLink().click();
	  return new SignInOrCreateAccountPage();
   }
}
	
