package com.capgemini.exercise.pages;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

public class BrowserFactory {
	private static final Logger log = Logger.getLogger(BrowserFactory.class);
	private static Map<String, WebDriver> drivers = new HashMap<String, WebDriver>();

	/*
	 * Factory method for getting browsers
	 * @return driver
	 */
	public static WebDriver getBrowser() {
		WebDriver driver = null;
		loadProperties();
		String browser = System.getProperty("browser");
		if (browser.equalsIgnoreCase("Firefox")) {
			driver = drivers.get("Firefox");
			if (driver == null) {
				System.setProperty("webdriver.gecko.driver", "geckodriver.exe");
				driver = new FirefoxDriver();
				drivers.put("Firefox", driver);
			}
		} else if (browser.equalsIgnoreCase("chrome"))
			driver = drivers.get("Chrome");
		if (driver == null) {
			System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
			driver = new ChromeDriver();
			drivers.put("Chrome", driver);
		} else if (browser.equalsIgnoreCase("IE")) {
			driver = drivers.get("IE");
			if (driver == null) {
				System.setProperty("webdriver.ie.driver", "IEDriverServer.exe");
				driver = new InternetExplorerDriver();
				drivers.put("IE", driver);
			} else {
				log.info("We do not support the browser \"" + browser + "\" ");
			}
		}
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		return driver;
	}

	public static void closeAllDriver() {
		for (String key : drivers.keySet()) {
			drivers.get(key).close();
			drivers.get(key).quit();
		}
	}
	private static Properties loadProperties() {
		Properties properties = System.getProperties();
		File file = new File("src//test//resources//common.properties");
		FileInputStream fis = null;
		try {
			fis = new FileInputStream(file);
		} catch (FileNotFoundException e1) {
			System.out.println("File not found at specified location");
			e1.printStackTrace();
		}
		try {
			properties.load(fis);
		} catch (IOException e) {
			System.out.println("File not found ");
			e.printStackTrace();
		}
		return properties;
	}
	
}
