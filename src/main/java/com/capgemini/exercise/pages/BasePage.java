package com.capgemini.exercise.pages;

import org.openqa.selenium.WebDriver;

/**
 * @author SSURAKAN
 *
 */
public class BasePage {

	protected WebDriver driver = BrowserFactory.getBrowser();

	/**
	 * Quit the browser
	 */
	public void closeBrowser() {
		driver.quit();
	}
}
